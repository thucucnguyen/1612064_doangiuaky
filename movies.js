
window.onload= function(){
  loadHomePage();
}
$(document).ready( function() { 
  $('form').on('submit', searchButtonClick); 
});

async function loadHomePage(){ //popular movies
    const response = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=5f49bc283c8c604537f7bf3a0b23c99a`);
    loading();
    const rs = await response.json();
    $('#pagination-demo').twbsPagination({
      totalPages: rs.total_pages,
      visiblePages: 5,
      next:'Next',
      prev:'Prev',
      onPageClick:function(event,page){
        load_MoviesList(page);
      }
  });
}

async function load_MoviesList(page){
  const response = await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=5f49bc283c8c604537f7bf3a0b23c99a&page=${page}`);
  loading();
  const rs = await response.json();
    $('#main_content').empty();
     for(const movie of rs.results){
        $('#main_content').append(`
            <div class="col-md-4 py-1">
            <div class="card shadow h-100" onclick="loadMovieDetails(${movie.id})">
                <img src="https://image.tmdb.org/t/p/w500${movie.poster_path}" class="card-img-top" alt="..." >
                <div class="card-body">
                <h5 class="card-title">${movie.title}</h5>
                <p class="card-text">${movie.overview}</p>
                <p class="card-text">Vote average: ${movie.vote_average}</p>
                <p class="card-text">Release: ${movie.release_date}</p>
                </div>
            </div>
            </div>
        `)
        }
}


function loading(){
    $('#main_content').append(`<div class="text-center" id="loading">
    <div class="spinner-border" role="status" >
      <span class="sr-only">Loading...</span>
    </div>
  </div>`);
  }

async function searchButtonClick(e){  // search people and movies
    e.preventDefault();

    const queryString = $('form input').val();
    const response = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=5f49bc283c8c604537f7bf3a0b23c99a&query=${queryString}`);
    loading();
    const rs = await response.json();
    $('#main_content').empty();
    $('#homepagetitle').remove();
    $('#pagination-demo').remove();
    $('#pagination-demo1').twbsPagination({
      totalPages: rs.total_pages,
      visiblePages: 3,
      next:'Next',
      prev:'Prev',
      onPageClick: async function(event,page){
        const response1 = await fetch(`https://api.themoviedb.org/3/search/movie?api_key=5f49bc283c8c604537f7bf3a0b23c99a&query=${queryString}&page=${page}`);
        loading();
        $('#main_content').empty();
        const rs1 = await response1.json();
        for(const movie of rs1.results){
            $('#main_content').append(`
                <div class="col-md-4 py-1">
                <div class="card shadow h-100" onclick="loadMovieDetails(${movie.id})">
                    <img src="https://image.tmdb.org/t/p/w500${movie.poster_path}" class="card-img-top" alt="..." >
                    <div class="card-body">
                    <h5 class="card-title">${movie.title}</h5>
                    <p class="card-text">${movie.overview}</p>
                    <p class="card-text">Vote average: ${movie.vote_average}</p>
                    </div>
                </div>
                </div>
                </br>
                </br>
                </br>
            `)
            }
        }
        
  });
}



async function loadMovieDetails(id){
    const response = await fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=5f49bc283c8c604537f7bf3a0b23c99a`);
    loading();
    $('#main_content').empty();
    const mInfor = await response.json();
    var genres="";
    for(const g of mInfor.genres){
        genres=genres+" "+g.name;
    }
    $('#homepagetitle').remove();
    $('#pagination-demo').remove();
    $('#main_content').empty();
       $('#main_content').append(`
       <!--Main layout-->
       <main class="mt-5 pt-4">
         <div class="container dark-grey-text mt-5">
           <!--Grid row-->
           <div class="row wow fadeIn">
             <!--Grid column-->
             <div class="col-md-6 mb-4">
               <img src="https://image.tmdb.org/t/p/w500${mInfor.poster_path}" class="img-fluid" alt="poster">
             </div>
             <!--Grid column-->
 
             <!--Grid column-->
             <div class="col-md-6 mb-4">
 
               <!--Content-->
               <div class="p-4">
 
                 <div class="mb-3">
                   <h1>${mInfor.title}</h1>
                 </div>
 
                 <p class="lead">
                   <span>runtime: ${mInfor.runtime} minutes</span>
                   </br>
                   <span>budget: $ ${mInfor.budget}</span>
                   </br>
                   <span>release date: ${mInfor.release_date}</span>
                   </br>
                   <span>revenue: $ ${mInfor.revenue}</span>
                   </br>
                   <span>popularity: ${mInfor.popularity}</span>
                   </br>
                   <span>vote average: ${mInfor.vote_average}</span>
                   </br>
                   <span>genres: ${genres}</span>
                 </p>
 
                 <p class="lead font-weight-bold">overview</p>
                 <p>${mInfor.overview}</p>
                 <p class="lead font-weight-bold">cast</p>
                 <p id="cast">
                 

                 </p>
                 <p class="lead font-weight-bold">director</p>
                 <p id="crew">

                 </p>
               </div>
               <!--Content-->
 
             </div>
             <!--Grid column-->
 
           </div>
           <!--Grid row-->
 
           <hr>
 
           <!--Grid row-->
           <div class="row d-flex justify-content-center wow fadeIn">
 
             <!--Grid column-->
             <div class="col-md-6 text-center">
 
               <h4 class="my-4 h4">production of</h4>
 
               
 
             </div>
             <!--Grid column-->
 
           </div>
           <!--Grid row-->
 
           <!--Grid row-->
           <div class="row wow fadeIn" id="prodcomp">
 
             
 
           </div>
           <!--Grid row-->
 
         </div>
         <div class="panel-heading">
                    <h1>review</h1>
                </div>
                <div class="panel-body">   
                    <hr>
                    <ul class="media-list" id="reviewlist">
    
                    </ul>
                </div>
       </main>
       <!--Main layout-->
        `)
        const response1 = await fetch(`https://api.themoviedb.org/3/movie/${id}/reviews?api_key=5f49bc283c8c604537f7bf3a0b23c99a`);
        loading();
        $('#loading').remove();
        const rv = await response1.json();
        var i;
        var x=rv.total_pages;
        for (i = 1;i < Number(x)+1; i++) {
          const response2 = await fetch(`https://api.themoviedb.org/3/movie/${id}/reviews?page=${i}&api_key=5f49bc283c8c604537f7bf3a0b23c99a`);
          loading();
          $('#loading').remove();
          const rvperpage = await response2.json();
          for(const review of rvperpage.results){
            console.log(i);
            $('#reviewlist').append(`
              <li class="media">
                              
              <div class="media-body">
                  <strong class="text-success">Author: ${review.author}</strong>
                  <p>${review.content}</p>
              </div>
            </li>
            `)
          }
        }
        for(const prodcomp of mInfor.production_companies){
            $('#prodcomp').append(`
            <div class="col-lg-4 col-md-6 mb-4">
                <img src="https://image.tmdb.org/t/p/original${prodcomp.logo_path}" class="img-fluid" alt="${prodcomp.name}" style="width:160px;height:120px;">                
            </div>
            `)
        }
        const response3 = await fetch(`https://api.themoviedb.org/3/movie/${id}/credits?api_key=5f49bc283c8c604537f7bf3a0b23c99a`);
        loading();
        $('#loading').remove();
        const credit = await response3.json(); 
        var i=1;   
        for(const cast of credit.cast)
        {
          $('#cast').append(`
          <button type="button" class="btn btn-light" onclick="loadCastInfor(${cast.id})">${cast.name}</button>
          `)
          i=i+1;
          if(i==4)
          {
            break;
          }
        }
        for(const crew of credit.crew){
          var jobString=crew.job;
          if(jobString=="Director")
          {
            $('#crew').append(`
            <p>${crew.name}</p>
            `)
          }
        }

}
async function loadCastInfor(id){
  const response = await fetch(`https://api.themoviedb.org/3/person/${id}?api_key=5f49bc283c8c604537f7bf3a0b23c99a`);
    loading();
    $('#main_content').empty();
    const castDetail = await response.json();
    const response1 = await fetch(`https://api.themoviedb.org/3/person/${id}/images?api_key=5f49bc283c8c604537f7bf3a0b23c99a`);
    loading();
    $('#main_content').empty();
    const castImage = await response1.json();
    var imgString="";
    var i=1;   
        for(const castimg of castImage.profiles)
        {
          i=i+1;
          imgString=imgString+castimg.file_path;
          if(i==2)
          {
            break;
          }
        }
    $('#homepagetitle').remove();
    $('#pagination-demo').remove();
    $('#main_content').empty();
       $('#main_content').append(`
       <!--Main layout-->
       <main class="mt-5 pt-4">
         <div class="container dark-grey-text mt-5">
           <!--Grid row-->
           <div class="row wow fadeIn">
             <!--Grid column-->
             <div class="col-md-6 mb-4">
               <img src="https://image.tmdb.org/t/p/w500${imgString}" class="img-fluid" alt="img">
             </div>
             <!--Grid column-->
 
             <!--Grid column-->
             <div class="col-md-6 mb-4">
 
               <!--Content-->
               <div class="p-4">
 
                 <div class="mb-3">
                   <h1>${castDetail.name}</h1>
                 </div>
 
                 <p class="lead">
                   <span>birthday: ${castDetail.birthday}</span>
                   </br>
                   
                   <span>born on: ${castDetail.place_of_birth}</span>
                   </br>
                   <span>popularity: ${castDetail.popularity}</span>
                   </br>
                   
                 </p>
 
                 <p class="lead font-weight-bold">biography</p>
                 <p>${castDetail.biography}</p>
                 

               </div>
               <!--Content-->
 
             </div>
             <!--Grid column-->
 
           </div>
           <!--Grid row-->
 
           <hr>
 
           
 
         </div>
         <div class="row d-flex justify-content-center wow fadeIn">
 
             <!--Grid column-->
             <div class="col-md-6 text-center">
 
               <h4 class="my-4 h4">related movies</h4>
 
               
 
             </div>
             <!--Grid column-->
 
           </div>
           <!--Grid row-->
 
           <!--Grid row-->
           <div class="row wow fadeIn" id="relMovies">

             
 
           </div>
           <!--Grid row-->
 
         </div>
       </main>
       <!--Main layout-->
        `)
        const response2 = await fetch(`https://api.themoviedb.org/3/person/${id}/movie_credits?api_key=5f49bc283c8c604537f7bf3a0b23c99a`);
        const listMovie = await response2.json();
        var i=1;
        for(const movie of listMovie.cast)
        {
          i=i+1;
          $('#relMovies').append(`
          <div class="col-md-4 py-1">
          <div class="card shadow h-100" onclick="loadMovieDetails(${movie.id})">
              <img src="https://image.tmdb.org/t/p/w500${movie.poster_path}" class="card-img-top" alt="..." >
              <div class="card-body">
              <h5 class="card-title">${movie.title}</h5>
              <p class="card-text">${movie.overview}</p>
              <p class="card-text">Vote average: ${movie.vote_average}</p>
              <p class="card-text">Release: ${movie.release_date}</p>
              </div>
          </div>
          </div>
          `)
          if(i==4)
          {
            break;
          }
        }

}